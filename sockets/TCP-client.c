#include <stdio.h>
#include <string.h> // For memset()
#include <unistd.h> // For close()

#include <sys/socket.h> // socket() and bind() 
#include <arpa/inet.h> // For htons

int main() {
  // AF_INET6 -- for IPv6
  // other options for second argument ("type") are SOCK_STREAM and SOCK_DGRAM
  // third argument is protocol
  // in this case it is 0
  int file_desc = socket(AF_INET6, SOCK_STREAM, 0);
  if (file_desc == -1)
    printf("Could not open a socket!\n");

  struct sockaddr_in6 my_address;
  memset(&my_address, 0, sizeof(struct sockaddr_in6));
  my_address.sin6_family = AF_INET6;
  my_address.sin6_port = htons(8000); // Converting to the right network byte order
  if (inet_pton(AF_INET6, "::FFFF:127.0.0.1", &my_address.sin6_addr) <= 0)
    printf("Not able to convert IP address to binary form!");

  int is_connected = connect(file_desc, (struct sockaddr*)&my_address, sizeof(struct sockaddr_in6));
  if (is_connected != 0)
    printf("Failed to connect to the TCP server!\n");
  else
    printf("Connected!\n");

  char* my_message = "Hello, TCP server!";
  if (write(file_desc, my_message, strlen(my_message)) != strlen(my_message))
    printf("Failed to send a message to the server!");

  if (close(file_desc) == -1)
    printf("Failed to close the connection to TCP server!\n");
}
