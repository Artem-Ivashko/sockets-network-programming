#include <stdio.h>
#include <string.h> // For memset()

#include <sys/socket.h> // socket() and bind() 
#include <arpa/inet.h> // For htons

#define MY_BUF_SIZE 100

int main() {
  int file_desc = socket(AF_INET6, SOCK_DGRAM, 0);

  printf("%d\n", file_desc);

  struct sockaddr_in6 my_address;
  memset(&my_address, 0, sizeof(struct sockaddr_in6));
  my_address.sin6_family = AF_INET6;
  my_address.sin6_port = htons(8000); // Converting to the right network byte order
  my_address.sin6_addr = in6addr_any; 
  
  if (bind(file_desc, (struct sockaddr*) &my_address, sizeof(struct sockaddr_in6)) == -1)
    printf("Not able to bind a socket!\n");

  char buffer[MY_BUF_SIZE];
  socklen_t length = sizeof(struct sockaddr_in6);
  if (recvfrom(file_desc, buffer, MY_BUF_SIZE, 0, (struct sockaddr*) &my_address,
               &length) == -1)
    printf("Failed to receive a message.\n");

  printf("The received message is: %s\n", buffer);
}
