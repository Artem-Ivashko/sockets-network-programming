#include <iostream>
#include <boost/asio.hpp>

using namespace std;
using boost::asio::ip::udp;

int main() {
  try {
    boost::asio::io_context io_context;
    udp::resolver resolver(io_context);
    udp::resolver::query query(udp::v4(), "localhost", "9000");
    udp::endpoint receiver_endpoint = *resolver.resolve(query);
    udp::socket socket(io_context);
    socket.open(udp::v4());

    vector<int> send_buf = {4, 5, 6};
    int size_to_send = send_buf.size();
    cout << "Size that the client is going to send: " << size_to_send << endl;
    socket.send_to(boost::asio::buffer(&size_to_send, sizeof(size_to_send)),
                   receiver_endpoint);
    socket.send_to(boost::asio::buffer(send_buf), receiver_endpoint);

    vector<char> recv_buf(100);
    udp::endpoint sender_endpoint;
    size_t len = socket.receive_from(boost::asio::buffer(recv_buf), sender_endpoint);
    cout.write(recv_buf.data(), len);
  }
  catch (std::exception& e) {
    cerr << e.what() << endl;
  }
}
