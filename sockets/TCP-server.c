#include <stdio.h>
#include <unistd.h> // For sleep()
#include <string.h> // For memset()

#include <sys/socket.h> // socket() and bind() 
#include <arpa/inet.h> // For htons

#define MY_BUF_SIZE 100

int main() {
  int file_desc = socket(AF_INET6, SOCK_STREAM, 0);

  printf("%d\n", file_desc);

  struct sockaddr_in6 my_address;
  memset(&my_address, 0, sizeof(struct sockaddr_in6));
  my_address.sin6_family = AF_INET6;
  my_address.sin6_port = htons(8000); // Converting to the right network byte order
  my_address.sin6_addr = in6addr_any; 
  
  if (bind(file_desc, (struct sockaddr*) &my_address, sizeof(struct sockaddr_in6)) == -1)
    printf("Not able to bind a socket!\n");

  /* Second argument to listen is the backlog, which is smth like the max number of connections */
  int my_backlog = 1;
  if (listen(file_desc, my_backlog) != 0)
    printf("Failed to listen for incoming connections!");

  struct sockaddr_in6 client_address;
  socklen_t addrlen = sizeof(struct sockaddr_storage);
    // Accept is a blocking call
  int client_fd = accept(file_desc, (struct sockaddr*) &client_address, &addrlen);
  if (client_fd == -1)
    printf("Error while establishing the connection!");
  else
    printf("Some client accepted.\n");

  char buffer[MY_BUF_SIZE];
  if (read(client_fd, buffer, MY_BUF_SIZE - 1) == -1)
    printf("Failed to receive a message.\n");
  else
    printf("The received message is: %s\n", buffer);

  if (close(client_fd) == -1 || close(file_desc) == -1)
    printf("Failed to close the connection to client!\n");
}
