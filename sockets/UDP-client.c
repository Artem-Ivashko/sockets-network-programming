#include <stdio.h>
#include <string.h> // For memset()

#include <sys/socket.h> // socket() and bind() 
#include <arpa/inet.h> // For htons

int main() {
  // see http://man7.org/linux/man-pages/man2/socket.2.html for more details
  // First argument is called "domain"
  // AF_INET stands for IPv4
  // AF_INET6 -- for IPv6
  // AF_UNIX -- for communication on the same host
  // other options for second argument ("type") are SOCK_STREAM and SOCK_DGRAM
  // third argument is protocol
  // in this case it is 0
  int file_desc = socket(AF_INET6, SOCK_DGRAM, 0);

  printf("%d\n", file_desc);

  struct sockaddr_in6 my_address;
  memset(&my_address, 0, sizeof(struct sockaddr_in6));
  my_address.sin6_family = AF_INET6;
  my_address.sin6_port = htons(8000); // Converting to the right network byte order
  if (inet_pton(AF_INET6, "::FFFF:127.0.0.1", &my_address.sin6_addr) <= 0)
    printf("Not able to convert IP address to binary form!");
  
  char* my_message = "Hello, TCP/IP!";
  if (sendto(file_desc, my_message, strlen(my_message), 0,
             (struct sockaddr*) &my_address, sizeof(struct sockaddr_in6)) !=
             strlen(my_message))
    printf("Not able to send the message!");
}
