#include <iostream>
#include <string>
#include <boost/asio.hpp>

using namespace std;
using boost::asio::ip::udp;

int main() {
  try {
    boost::asio::io_context io_context;
    const int port = 9000;
    udp::socket socket(io_context, udp::endpoint(udp::v4(), port));
    udp::endpoint remote_endpoint;
    int size_to_receive;
    socket.receive_from(boost::asio::buffer(&size_to_receive, sizeof(size_to_receive)), remote_endpoint);
    cout << "Server: my client has address " << remote_endpoint.address() << " and its port is " << remote_endpoint.port() << endl;
    cout << "Size of vector to be received: " << size_to_receive << endl;
    vector<int> recv_buf(size_to_receive);
    socket.receive_from(boost::asio::buffer(recv_buf), remote_endpoint);
    cout << "Vector received by the server: ";
    for(auto el: recv_buf)
      cout << el << " ";
    cout << endl;
    
    string message = "Hello, Inna!";
    boost::system::error_code ignored_error;
    socket.send_to(boost::asio::buffer(message),
                   remote_endpoint, 0, ignored_error);
  }
  catch (exception& e) {
    cerr << e.what() << std::endl;
  }
}
