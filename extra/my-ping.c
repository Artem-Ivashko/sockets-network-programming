#include <stdio.h>
#include <string.h> // For memset()

#include <sys/socket.h> // socket() and bind() 
//#include <netinet/ip_icmp.h> // Contains IPPROTO_ICMP definition,
// but anyway we do not use it here.
#include <netdb.h> // For getprotobyname
#include <arpa/inet.h> // For htons

int main() {
  struct protoent *proto;  
  proto = getprotobyname("icmp");
  // see http://man7.org/linux/man-pages/man2/socket.2.html for more details
  // First argument is called "domain"
  // AF_INET stands for IPv4
  // AF_INET6 -- for IPv6
  // AF_UNIX -- for communication on the same host
  // other options for second argument ("type") are SOCK_STREAM and SOCK_DGRAM
  // third argument is protocol
  // in this case it is 1, which is identical to IPPROTO_ICMP
  // IPPROTO_ICMP is said to fill the proper checksum in the ICMP header
  // Opening of ICMP socket requires sudo somehow...
  int file_desc = socket(AF_INET6, SOCK_RAW, proto->p_proto);

  printf("%d\n", file_desc);

  struct sockaddr_in6 my_address;
  memset(&my_address, 0, sizeof(struct sockaddr_in6));
  my_address.sin6_family = AF_INET6;
  my_address.sin6_port = htons(80); // Converting to the right network byte order
  inet_pton(AF_INET6, "127.0.0.1", &my_address.sin6_addr); 
  
  bind(file_desc, (struct sockaddr*) &my_address, sizeof(struct sockaddr_in6));

  
}
