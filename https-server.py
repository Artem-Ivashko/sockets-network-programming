# Python 2 version
# In Python 3, http.server must be used instead
import BaseHTTPServer, SimpleHTTPServer
import ssl

httpd = BaseHTTPServer.HTTPServer(('0.0.0.0', 8443), SimpleHTTPServer.SimpleHTTPRequestHandler)
#httpd.socket = ssl.wrap_socket(httpd.socket, certfile='path_to_my/cert.pem', keyfile='path_to_my/key.pem', server_side=True)

httpd.serve_forever()
