#include <iostream>

#include <NetworkManager.h>

int main() {
        NMClient* client = nm_client_new(NULL, NULL);
        NMDevice* device = nm_client_get_device_by_iface(client, "wlp3s0");
        NMDhcpConfig* config = nm_device_get_dhcp4_config(device);
        /* Source code is in https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.28.0/libnm/nm-dhcp-config.c */
        std::string option_value = nm_dhcp_config_get_one_option(config, "routers");
        std::cout << option_value << std::endl;
}
